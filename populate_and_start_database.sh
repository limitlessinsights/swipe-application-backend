#!/bin/sh
/var/lib/neo4j/bin/neo4j start && \
sleep 5 && \
cypher-shell -u "neo4j" -p "neo4j" "CALL dbms.changePassword('passme')" && \
cat create_database.cypher | cypher-shell -u "neo4j" -p "passme"

