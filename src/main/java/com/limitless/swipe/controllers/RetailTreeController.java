package com.limitless.swipe.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.limitless.swipe.model.JsonModel;
import com.limitless.swipe.model.Product;
import com.limitless.swipe.repo.ProductRepository;

@RestController
public class RetailTreeController {

	private final static Logger log = LoggerFactory.getLogger(RetailTreeController.class);
	@Autowired ProductRepository personRepository;
	
	@RequestMapping("/retailtree")
	@ResponseStatus(HttpStatus.OK)
	public JsonModel getRetailTree() {
		Iterable<Product> family = personRepository.findAll();
		Product first = family.iterator().next();
		JsonModel me = recurse(first);
		return me;
	}

	private JsonModel recurse(Product first) {
	   JsonModel json = new JsonModel();
       json.setName(first.getName());
       json.setImage(first.getImage());
       json.setQuestion(first.getQuestion());
       json.setRecommendation(first.getRecommendation());
       if (first.likes != null) {
         json.setLikes(recurse(first.likes.iterator().next()));
       }
       if (first.dislikes != null) {
           json.setDislikes(recurse(first.dislikes.iterator().next()));
         }
       return json;
	}
}
