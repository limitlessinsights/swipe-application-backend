package com.limitless.swipe;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.limitless.swipe.model.JsonModel;
import com.limitless.swipe.model.Product;
import com.limitless.swipe.repo.ProductRepository;

@SpringBootApplication
@EnableNeo4jRepositories
public class SwipeApplication {

	private final static Logger log = LoggerFactory.getLogger(SwipeApplication.class);

	public static void main(String[] args) throws Exception {
		System.out.println("DEMO RUNNING MAIN");
		SpringApplication.run(SwipeApplication.class, args);
	}

	/*@Bean
	CommandLineRunner demo(SopranosRepository personRepository) {
		return args -> {

			personRepository.deleteAll();

			Sopranos greg = new Sopranos("Greg");
			Sopranos roy = new Sopranos("Roy");
			Sopranos craig = new Sopranos("Craig");

			List<Sopranos> team = Arrays.asList(greg, roy, craig);

			log.info("Before linking up with Neo4j...");

			team.stream().forEach(person -> log.info("\t" + person.toString()));

			personRepository.save(greg);
			personRepository.save(roy);
			personRepository.save(craig);

			greg = personRepository.findByName(greg.getName());
			greg.likes(roy);
			greg.likes(craig);
			personRepository.save(greg);

			roy = personRepository.findByName(roy.getName());
			roy.likes(craig);
			// We already know that roy works with greg
			personRepository.save(roy);

			// We already know craig works with roy and greg

			log.info("Lookup each person by name...");
			team.stream().forEach(person -> log.info(
					"\t" + personRepository.findByName(person.getName()).toString()));
		};
	}*/
	
	/*@Bean
	CommandLineRunner demo(SopranosRepository personRepository) {
		return args -> {
			log.info("Lookup each person by name...");
			log.info(personRepository.findByName("tony").toString());
			Iterable<Sopranos> family = personRepository.findAll();
			for (Sopranos soprano : family) {
				log.info("Name:" + soprano.getName());
				if (soprano.likes != null) {
				  log.info("Likes:" + soprano.likes.iterator().next().getName());
				}
				if (soprano.dislikes != null) {
				  log.info("Dislikes:" + soprano.dislikes.iterator().next().getName());
				}
			}
			Sopranos first = family.iterator().next();
			JsonModel me = recurse(first);
			ObjectMapper mapper = new ObjectMapper();
			String jsonInString = mapper.writeValueAsString(me);
			log.info(jsonInString);
		};
	}

	private JsonModel recurse(Sopranos first) {
	   JsonModel json = new JsonModel();
       json.setName(first.getName());
       json.setImage(first.getImage());
       if (first.likes != null) {
         json.setLikes(recurse(first.likes.iterator().next()));
       }
       if (first.dislikes != null) {
           json.setDislikes(recurse(first.dislikes.iterator().next()));
         }
       return json;
	}*/
	
	/*@Bean
	CommandLineRunner demo(SopranosRepository sopranosRepository) {
		System.out.println("DEMO RUNNING NOW");
		Sopranos tony = sopranosRepository.findByName("tony");
		System.out.println(tony);
		System.out.println(sopranosRepository.count());
		System.out.println(sopranosRepository.findAll());
		System.out.println("printed");
		for (Sopranos soprano : all) {
			System.out.println("COOL");
			System.out.println(soprano.getName());
		}
		return args -> {
			for (Sopranos soprano : all) {
				System.out.println("COOL");
				System.out.println(soprano.getName());
			}
		};
	}*/
}