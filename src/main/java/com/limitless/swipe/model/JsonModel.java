package com.limitless.swipe.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JsonModel {
	
	@JsonProperty("name")
	private String name;
	
	@JsonProperty("image")
	private String image;
	
	@JsonProperty("likes")
	private JsonModel likes;
	
	@JsonProperty("dislikes")
	private JsonModel dislikes;
	
	@JsonProperty("question")
	private String question;
	
	@JsonProperty("recommendation")
	private String recommendation;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public JsonModel getLikes() {
		return likes;
	}
	public void setLikes(JsonModel likes) {
		this.likes = likes;
	}
	public JsonModel getDislikes() {
		return dislikes;
	}
	public void setDislikes(JsonModel dislikes) {
		this.dislikes = dislikes;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	
	public String getRecommendation() {
		return recommendation;
	}
	public void setRecommendation(String recommendation) {
		this.recommendation = recommendation;
	}

	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
}