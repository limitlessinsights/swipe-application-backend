package com.limitless.swipe.model;

import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

@NodeEntity
public class Product {

	@Id @GeneratedValue private Long id;

	private String name;
	
	private String question;

	private String image;
	
	private String recommendation;

	private Product() {
		// Empty constructor required as of Neo4j API 2.0.5
	};

	public Product(String name) {
		this.name = name;
	}

	/**
	 * Neo4j doesn't REALLY have bi-directional relationships. It just means when querying
	 * to ignore the direction of the relationship.
	 * https://dzone.com/articles/modelling-data-neo4j
	 */
	@Relationship(type = "like", direction = Relationship.OUTGOING)
	public Set<Product> likes;
	@Relationship(type = "dislike", direction = Relationship.OUTGOING)
	public Set<Product> dislikes;

	public void likes(Product product) {
		if (likes == null) {
			likes = new HashSet<>();
		}
		likes.add(product);
	}
	
	public void disLikes(Product product) {
		if (likes == null) {
			likes = new HashSet<>();
		}
		likes.add(product);
	}

	public String toString() {
		return this.name + "'s likes => "
			+ Optional.ofNullable(this.likes).orElse(
					Collections.emptySet()).stream()
						.map(Product::getName)
						.collect(Collectors.toList()) + 
						"and dislikes => "
						+ Optional.ofNullable(this.dislikes).orElse(
								Collections.emptySet()).stream()
									.map(Product::getName)
									.collect(Collectors.toList());
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getRecommendation() {
		return recommendation;
	}

	public void setRecommendation(String recommendation) {
		this.recommendation = recommendation;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}
}