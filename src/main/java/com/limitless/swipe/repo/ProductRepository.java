package com.limitless.swipe.repo;

import org.springframework.data.repository.CrudRepository;

import com.limitless.swipe.model.Product;

public interface ProductRepository extends CrudRepository<Product, Long> {
	Product findByName(String name);	
}