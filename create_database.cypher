CREATE (a: Product {name: "fashion", question: "Fashion ", image : "fashion.jpg"})
, (b: Product {name: "footwear", question: "Footwear ", image : "footwear.jpg"})
, (c: Product {name: "clothes", question: "Clothes ", image : "clothes.jpg"})
, (d: Product {name: "evening", question: "Evening Wear ", image : "evening.jpg"})
, (e: Product {name: "street", question: "Street Trainers ", image : "street.jpg" ,recommendation: "recommendation.jpg"})
, (f: Product {name: "casual", question: "Casual Wear ", image : "casual.jpg"})
, (g: Product {name: "trainers", question: "Trainers ", image : "trainers.jpg"})
, (h: Product {name: "formal", question: "Formal Footwear ", image : "formal.jpg"})
, (i: Product {name: "sports", question: "Sport Footwear ", image : "sports.jpg" ,recommendation: "recommendation.jpg"})
, (j: Product {name: "inside", question: "Inside Casual ", image : "inside.jpg" ,recommendation: "recommendation.jpg"})
, (k: Product {name: "outside", question: "Outside Casual ", image : "outside.jpg" ,recommendation: "recommendation.jpg"})
, (l: Product {name: "work", question: "work wear", image : "work.jpg" ,recommendation: "recommendation.jpg" })
, (m: Product {name: "play", question: "play wear ", image : "play.jpg" ,recommendation: "recommendation.jpg" })
, (n: Product {name: "dinner", question: "Dinner Wear ", image : "dinner.jpg" ,recommendation: "recommendation.jpg"})
, (o: Product {name: "nightclub", question: "NightClub wear ", image : "nightclub.jpg" ,recommendation: "recommendation.jpg"});



MATCH (a:Product {name:"fashion"}),(b:Product {name:"clothes"}) CREATE (a)-[r:like]->(b) return type(r);
MATCH (a:Product {name:"fashion"}),(b:Product {name:"footwear"}) CREATE (a)-[r:dislike]->(b) return type(r);

MATCH (a:Product {name:"clothes"}),(b:Product {name:"casual"}) CREATE (a)-[r:like]->(b) return type(r);
MATCH (a:Product {name:"clothes"}),(b:Product {name:"evening"}) CREATE (a)-[r:dislike]->(b) return type(r);

MATCH (a:Product {name:"footwear"}),(b:Product {name:"trainers"}) CREATE (a)-[r:like]->(b) return type(r);
MATCH (a:Product {name:"footwear"}),(b:Product {name:"formal"}) CREATE (a)-[r:dislike]->(b) return type(r);

MATCH (a:Product {name:"trainers"}),(b:Product {name:"sports"}) CREATE (a)-[r:like]->(b) return type(r);
MATCH (a:Product {name:"trainers"}),(b:Product {name:"street"}) CREATE (a)-[r:dislike]->(b) return type(r);

MATCH (a:Product {name:"casual"}),(b:Product {name:"outside"}) CREATE (a)-[r:like]->(b) return type(r);
MATCH (a:Product {name:"casual"}),(b:Product {name:"inside"}) CREATE (a)-[r:dislike]->(b) return type(r);

MATCH (a:Product {name:"formal"}),(b:Product {name:"work"}) CREATE (a)-[r:like]->(b) return type(r);
MATCH (a:Product {name:"formal"}),(b:Product {name:"play"}) CREATE (a)-[r:dislike]->(b) return type(r);

MATCH (a:Product {name:"evening"}),(b:Product {name:"nightclub"}) CREATE (a)-[r:like]->(b) return type(r);
MATCH (a:Product {name:"evening"}),(b:Product {name:"dinner"}) CREATE (a)-[r:dislike]->(b) return type(r);

