#Limitness insights swipe back end

> A project to serve the json decision tree for products to swipe on

#Build setup

First Build the project. The output jar file will be located in the target directory
```
mvn clean install
```

Build the docker image in the project directory.
```
sudo docker build -t limitless-swipe .
```

Run the docker image. Locally the website will be located at link: http://localhost:8081/#/
```
sudo docker run  -it --publish=7474:7474 --publish=7687:7687 --publish=8081:8081 --publish=8080:8080 limitless-swipe /bin/bash
```

When the docker image starts up it will run this script first. This will start and populate the database.
In the docker image you must start the database and the sprint boot jar file manually. N.B this is only because starting the neo4j server and then starting spring does not work in one script. 
```
./populate_and_start_database.sh && ./start_server.sh
```

To view the database use this link. username: neo4j and password: passme
```
http://localhost:7474/browser/
```
