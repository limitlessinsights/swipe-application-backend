FROM neo4j 
EXPOSE 8080 7474 7473 7687
COPY target/swipe-0.0.1-SNAPSHOT.jar  app.jar
ADD public public
COPY populate_and_start_database.sh populate_and_start_database.sh 
COPY start_server.sh start_server.sh
COPY create_database.cypher create_database.cypher
